#!/bin/bash

DOCKER_IMAGE_NAME="geipan-statistics-builder"

COMMAND_RUN_TESTS="conda run --name python36 py.test -s"

docker build -t $DOCKER_IMAGE_NAME .
docker run -v `pwd`/:/geipan-statistics-builder -i -t $DOCKER_IMAGE_NAME bash -c "$COMMAND_RUN_TESTS"
