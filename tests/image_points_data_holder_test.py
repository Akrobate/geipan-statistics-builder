import unittest
from src.image_points_data_holder import ImagePointsDataHolder

class TestImagePointsDataHolder(unittest.TestCase):

    def test_ExpectedColumns(self):        
        image_point_data_holder = ImagePointsDataHolder()
        df = image_point_data_holder.getPointList()
        self.assertListEqual(df.columns.values.tolist(), ['longitude', 'latitude', 'markersize', 'alpha'])

    def test_AddPointToList(self):
        image_point_data_holder = ImagePointsDataHolder()
        image_point_data_holder.addPoint(1, 2, 3, 4)
        point_list = image_point_data_holder.getPointList()
        assert point_list.loc[0, ['longitude']].values.tolist()[0] == 1
        assert point_list.loc[0, ['latitude']].values.tolist()[0] == 2
        assert point_list.loc[0, ['markersize']].values.tolist()[0] == 3
        assert point_list.loc[0, ['alpha']].values.tolist()[0] == 4

    def test_processFrame(self):
        image_point_data_holder = ImagePointsDataHolder()

        image_point_data_holder.setAlphaCoef(2)
        image_point_data_holder.setSizeCoef(2)
        image_point_data_holder.addPoint(1, 2, 3, 4)
        image_point_data_holder.addPoint(2, 3, 10, 4)
        point_list = image_point_data_holder.getPointList()
        (rows_count, _) = point_list.shape
        assert rows_count == 2
        
        image_point_data_holder.processFrame()
        (rows_count, _) = point_list.shape
        assert rows_count == 2

        assert point_list['markersize'][0] == 5
        assert point_list['markersize'][1] == 12

        assert point_list['alpha'][0] == 2
        assert point_list['alpha'][1] == 2

        image_point_data_holder.processFrame()
        (rows_count, _) = point_list.shape
        assert rows_count == 0
