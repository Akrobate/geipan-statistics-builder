import unittest
import pandas as pd
from _pytest.monkeypatch import MonkeyPatch
# from src.geipan_data import loadLongitudeLatitudeDateTestimony, prepareTestimonyData
from src.geipan_data import GeipanData

class TestGeipanData(unittest.TestCase):

    def setUp(self):
        self.monkeypatch = MonkeyPatch()
        self.geipan_data = GeipanData()
        self.geipan_data.setTestimonyFilePath('./data/geipan/temoignages_pub.csv')

    def test_loadPrepareTestimonyData(self):
        
        def read_csv(*args, **kwargs):
            return pd.DataFrame({
                'obs_date_heure': ['2010-05-21', '1926-05-21'],
                'obs_1_lon': [45.123, 78.4564],
                'obs_1_lat': [32.123, 98.4564],
            })

        self.monkeypatch.setattr(pd, 'read_csv', read_csv)

        df = self.geipan_data.loadPrepareTestimonyData()
        self.assertListEqual(
            df.columns.values.tolist(),
            ['longitude', 'latitude']
        )

    def test_loadLongitudeLatitudeDateTestimony(self):

        def read_csv(*args, **kwargs):
            return pd.DataFrame({
                'obs_date_heure': ['2010-05-21', '1926-05-21'],
                'obs_1_lon': [45.123, 78.4564],
                'obs_1_lat': [32.123, 98.4564],
            })

        self.monkeypatch.setattr(pd, 'read_csv', read_csv)

        df = self.geipan_data.loadLongitudeLatitudeDateTestimony()

        self.assertListEqual(
            df.columns.values.tolist(),
            ['obs_date_heure', 'obs_1_lon', 'obs_1_lat']
        )

    def test_prepareTestimonyData(self):
        seed = pd.DataFrame({
            'obs_date_heure': ['2010-05-21', '1926-05-21'],
            'obs_1_lon': [45.123, 78.4564],
            'obs_1_lat': [32.123, 98.4564],
        })
        df = self.geipan_data.prepareTestimonyData(seed)
        self.assertListEqual(
            df.columns.values.tolist(),
            ['longitude', 'latitude']
        )