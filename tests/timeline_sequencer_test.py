import unittest
import pandas as pd
from _pytest.monkeypatch import MonkeyPatch
from src.timeline_sequencer import TimelineSequencer

class TestTimelineSequencer(unittest.TestCase):

    def setUp(self):
        self.monkeypatch = MonkeyPatch()
        data_source_seed_index = index = pd.DatetimeIndex(['2010-05-01', '2014-05-02', '2014-05-01', '2015-07-04', '2016-05-01'])

        self.data_source_seed = pd.DataFrame(
            {
                'logitude': [1, 2, 3, 4, 5],
                'latitude': [1, 2, 3, 4, 5]
            },
            index = data_source_seed_index
        )

    def test_initSequence(self):
        timeline_sequencer = TimelineSequencer()
        timeline_sequencer.setSourceData(self.data_source_seed)
        timeline_sequencer.initSequence('Y')
        assert len(timeline_sequencer.date_range) == 6 

    def test_getNextFrame(self):
        timeline_sequencer = TimelineSequencer()
        timeline_sequencer.setSourceData(self.data_source_seed)
        timeline_sequencer.initSequence('Y')

        sequence_1 = timeline_sequencer.getNextFrame()
        assert len(sequence_1.index) == 0
        
        sequence_2 = timeline_sequencer.getNextFrame()
        assert len(sequence_2.index) == 0
        
        sequence_3 = timeline_sequencer.getNextFrame()
        assert len(sequence_3.index) == 0

        sequence_4 = timeline_sequencer.getNextFrame()
        assert len(sequence_4.index) == 2

    def test_nextFrameExists(self):
        timeline_sequencer = TimelineSequencer()
        timeline_sequencer.setSourceData(self.data_source_seed)
        timeline_sequencer.initSequence('4Y')

        assert timeline_sequencer.nextFrameExists() == True
        
        timeline_sequencer.getNextFrame()
        assert timeline_sequencer.nextFrameExists() == False


    def test_getCurrentFrameStartDate(self):
        timeline_sequencer = TimelineSequencer()
        timeline_sequencer.setSourceData(self.data_source_seed)
        timeline_sequencer.initSequence('YS')

        assert timeline_sequencer.getCurrentFrameStartDate() == '2011-01-01 00:00:00'

        timeline_sequencer.getNextFrame()
        assert timeline_sequencer.getCurrentFrameStartDate() == '2012-01-01 00:00:00'
        assert timeline_sequencer.getCurrentFrameStartDate('%Y-%m-%d') == '2012-01-01'


