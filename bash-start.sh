#!/bin/bash

DOCKER_IMAGE_NAME="geipan-statistics-builder"

docker build -t $DOCKER_IMAGE_NAME .
docker run -v `pwd`/:/geipan-statistics-builder -i -t $DOCKER_IMAGE_NAME bash

