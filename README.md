# geipan-statistics-builder

Source data from:

http://www.cnes-geipan.fr/


Data is available on

http://www.cnes-geipan.fr/index.php?id=202

## Files to update datas

### Cases

http://www.cnes-geipan.fr/fileadmin/documents/cas_pub.csv

### Temoignages

http://www.cnes-geipan.fr/fileadmin/documents/temoignages_pub.csv


## Tech notes

### Export current packages list

```sh
conda run --name python36 conda list -e > conda_requirements.txt
```

### Dependencies for the project

```sh
RUN conda create --name python36 python=3.6

RUN conda install --name python36 cartopy
RUN conda install --name python36 pandas
RUN conda install --name python36 numpy
RUN conda install --name python36 seaborn
RUN conda install --name python36 scikit-learn
RUN conda install --name python36 pytest
```