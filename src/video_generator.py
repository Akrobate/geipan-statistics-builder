import cv2
import numpy as np
import glob
import os

class VideoGenerator():

    def __init__(self, images_path, images_extention):
        self.images_path = None
        self.images_extention = None


    def generateVideo(self, output_file, fps = 15):
        img_array = []

        path = os.path.join(self.images_path, self.images_extention)

        for filename in glob.glob(path):
            img = cv2.imread(filename)
            height, width, layers = img.shape
            size = (width,height)
            img_array.append(img)

        out = cv2.VideoWriter(
            output_file,
            cv2.VideoWriter_fourcc(*'DIVX'),
            fps, 
            size
        )
        
        for i in range(len(img_array)):
            out.write(img_array[i])
        out.release()
    