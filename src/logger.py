from datetime import datetime

def info(message):
    _output(message)

def error(message):
    _output(message)

def _output(message):
    printConsole(timeWrapper(message))

def timeWrapper(message):
    date_time_str = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return date_time_str + ' - ' + message

def printConsole(message):
    print(message)