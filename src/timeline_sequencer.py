import pandas as pd

class TimelineSequencer():

    def __init__(self):
        self.data = None
        self.date_range = None
        self.current_daterange_index = 0
        self.data_time_format = '%Y-%m-%d %H:%M:%S'

    def setSourceData(self, data):
        self.data = data

    def initSequence(self, frequency):
        start_date = self.data.index.min().strftime('%Y-%m-%d') + ' 00:00:00'
        end_date = self.data.index.max().strftime('%Y-%m-%d') + ' 00:00:00'
        self.date_range = pd.date_range(start_date, end_date, freq=frequency).to_pydatetime()


    def getNextFrame(self):
        lower_boundary = self.date_range[self.current_daterange_index].strftime(self.data_time_format)
        upper_boundary = self.date_range[self.current_daterange_index + 1].strftime(self.data_time_format)
        self.current_daterange_index = self.current_daterange_index + 1
        return self.data.loc[lower_boundary:upper_boundary].copy()

    def getCurrentFrameStartDate(self, date_format = '%Y-%m-%d %H:%M:%S'):
        return self.date_range[self.current_daterange_index].strftime(date_format)

    def nextFrameExists(self):
        if (self.current_daterange_index + 1 >= len(self.date_range)):
            return False
        return True