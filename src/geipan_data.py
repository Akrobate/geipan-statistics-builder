import pandas as pd

class GeipanData():

    def __init__(self):
        self.testimony_file_path = ''

    def setTestimonyFilePath(self, file_path):
        self.testimony_file_path = file_path

    def loadPrepareTestimonyData(self):
        loaded_data = self.loadLongitudeLatitudeDateTestimony()
        prepared_data = self.prepareTestimonyData(loaded_data)
        return prepared_data

    def prepareTestimonyData(self, df):
        df['obs_date_heure'] = pd.to_datetime(df['obs_date_heure'], errors='coerce')
        df.rename(columns = {
            'obs_date_heure':'datetime',
            'obs_1_lon': 'longitude',
            'obs_1_lat': 'latitude',
        }, inplace=True)
        df.set_index('datetime', inplace=True)
        df.sort_index(ascending=True, inplace=True)
        df = self.dropNanLongitudeLatitude(df)
        return df

    def dropNanLongitudeLatitude(self, df):
        return df[df['longitude'].notnull() & df['latitude'].notnull()]

    def loadLongitudeLatitudeDateTestimony(self):
        selected_columns = [
            'obs_date_heure',
            'obs_1_lat',
            'obs_1_lon'
        ]
        return self.loadCsvGeipanFile(self.testimony_file_path, selected_columns)

    def loadCsvGeipanFile(self, file_path, fields_list):
        return pd.read_csv(
            file_path,
            sep = ';',
            encoding = "ISO-8859-1",
            usecols = fields_list
        )
