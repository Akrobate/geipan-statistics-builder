import os
import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import cartopy.feature as cfeature

class MapImageGenerator:

    def __init__(self):
        self.figure = None
        self.figure_size = None
        self.ax_backgorund_map = None
        self.ax_plots = None
        self.crs = None
        self.stamen_terrain = None
        self.map_zone_coords = None
        self.initMap()

    def setVisibleCoords(self, coords_map_list):
        self.map_zone_coords = coords_map_list

    def setVisibleCoordsToFrance(self):
        self.setVisibleCoords([-5, 9, 51, 42])

    def initMap(self):
        self.figure_size = [10, 8]
        self.figure = plt.figure(figsize = self.figure_size)
        self.stamen_terrain = cimgt.Stamen('terrain-background')
        self.crs = ccrs.Geodetic()

    def initMapBackgroundLayer(self):
        self.ax_backgorund_map = self.figure.add_subplot(1, 1, 1, projection=self.stamen_terrain.crs)
        self.ax_backgorund_map.set_extent(self.map_zone_coords, crs=self.crs)
        self.ax_backgorund_map.add_image(self.stamen_terrain, 8)
        self.ax_backgorund_map.add_feature(cfeature.COASTLINE)
        self.ax_backgorund_map.add_feature(cfeature.BORDERS, color='black')
        return self.ax_backgorund_map

    def drawPlotsLayer(self, plots_dataframe):
        self.ax_plots = self.figure.add_axes(
            self.ax_backgorund_map.get_position(original=True),
            frameon=False,
            projection=self.stamen_terrain.crs
        )
        self.ax_plots.set_extent(self.map_zone_coords, crs=self.crs)
        plots_dataframe.apply(self.drawOnePlotOnPlotsLayer, axis = 1)

    
    def drawOnePlotOnPlotsLayer(self, plot_data):
        self.ax_plots.plot(
            plot_data.longitude,
            plot_data.latitude,
            alpha=plot_data.alpha,
            markersize=plot_data.markersize,
            marker='o',
            color='red',
            linestyle="None",
            transform=self.crs
        )

    def clearPlotsLayer(self):
        self.ax_plots.cla()
        self.ax_plots = None

    def saveToImage(self, file):
        plt.savefig(file)

    def show(self):
        plt.show()

    def removeImagesFromFolder(self, images_path, image_extention):
        path = os.path.join(images_path, image_extention)
        for filename in glob.glob(path):
            if os.path.exists(filename):
                os.remove(filename)

