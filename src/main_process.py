import os
import pandas as pd
from src.logger as logger
from src.geipan_data import GeipanData
from src.self.map_image_generator import MapImageGenerator
from src.image_points_data_holder import ImagePointsDataHolder
from src.timeline_sequencer import TimelineSequencer
# from src.video_generator import VideoGenerator


class mainProcess:

    def __init__(self, source_file, images_folder, videos_file):
        self.map_image_generator = MapImageGenerator()

        self.source_csv_file = source_file
        self.images_data_folder = images_folder
        self.output_video_file = videos_file
        self.output_image_name_prefix = 'test-'
        self.output_image_extention = '.png'
        
        self.video_fps = 15
        self.start_date_filter = '1970'
        self.end_date_filter = '2020'
        self.sequence_period = 'Y'


    def generateImagesFromData(self):
        # Init modules
        geipan_data = GeipanData()
        sequencer = TimelineSequencer()
        image_points_data_holder = ImagePointsDataHolder()

        self.map_image_generator.setVisibleCoordsToFrance()
        self.map_image_generator.initMapBackgroundLayer()

        # Loading data
        geipan_data.setTestimonyFilePath(self.source_csv_file)
        prepared_data = geipan_data.loadPrepareTestimonyData()

        # Subselecting data
        prepared_data = prepared_data.loc[self.start_date_filter:self.end_date_filter]

        logger.info(prepared_data)

        # Initing / parameter sequencer
        sequencer.setSourceData(prepared_data)
        sequencer.initSequence(self.sequence_period)

        output_image_count = 1
        
        # Processing loop of each frame
        while sequencer.nextFrameExists():

            current_data_points = sequencer.getNextFrame()

            # Current points setted to dataholder
            if (current_data_points.shape[0] > 0):
                image_points_data_holder.addPointList(
                    current_data_points['longitude'],
                    current_data_points['latitude']
                )

            # Dataholder frame process
            image_points_data_holder.processFrame()
            
            # Image Map Generator setted
            self.map_image_generator.drawPlotsLayer(
                image_points_data_holder.getPointList()
            )

            # Save image file to image folder from Image Map Generator
            self.map_image_generator.saveToImage(
                os.path.join(
                    self.images_data_folder, 
                    self.output_image_name_prefix + str(output_image_count).zfill(8) + '.png'
                )
            )
            self.map_image_generator.clearPlotsLayer()
            output_image_count += 1
        
        logger.info('Generation finished')

    def generateVideoFromImages(self):
        generator = VideoGenerator(self.images_data_folder, '*' + self.output_image_extention)
        generator.generateVideo(self.output_video_file, self.video_fps)

    def cleanPreviousImageGeneration(self):
        self.map_image_generator.removeImagesFromFolder(self.images_data_folder, '*' + self.output_image_extention)

    def setSequencePeriod(self, sequence_period):
        self.sequence_period = sequence_period
    
    def setDateRange(self, start_date_filter, end_date_filter):
        self.start_date_filter = start_date_filter
        self.end_date_filter = end_date_filter
    
    def setVideoFps(self, fps):
        self.video_fps = fps