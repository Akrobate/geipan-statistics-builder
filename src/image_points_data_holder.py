import pandas as pd

class ImagePointsDataHolder:

    def __init__(self):
        columns = ['longitude', 'latitude', 'markersize', 'alpha']
        self.point_list = pd.DataFrame(columns = columns)
        self.alpha_coef = 0.01
        self.size_coef = 1
        self.initial_markersize = 1
        self.initial_alpha = 1.0

    def addPoint(self, longitude, latitude, size = None, alpha = None):
        _size = self.initial_markersize
        _alpha = self.initial_alpha

        if size != None:
            _size = size

        if alpha != None:
            _alpha = alpha

        new_point_df = pd.DataFrame([{
            'longitude': longitude,
            'latitude': latitude,
            'markersize': _size,
            'alpha': _alpha
        }])
        self.point_list = self.point_list.append(new_point_df, ignore_index=True)


    def addPointList(self, longitude, latitude, size = None, alpha = None):
        _size = self.initial_markersize
        _alpha = self.initial_alpha

        if size != None:
            _size = size

        if alpha != None:
            _alpha = alpha

        new_point_df = pd.DataFrame({
            'longitude': longitude,
            'latitude': latitude,
            'markersize': _size,
            'alpha': _alpha
        })
        self.point_list = self.point_list.append(new_point_df, ignore_index=True)



    def getPointList(self):
        return self.point_list

    def processFrame(self):
        # process all points transformation
        self.updatePointsValues()
        self.dropNonVisiblePoints()
        
    def dropNonVisiblePoints(self):
        point_drop_list = self.point_list[self.point_list.alpha <= 0]
        self.point_list.drop(point_drop_list.index, inplace=True)

    def updatePointsValues(self):
        self.point_list.alpha = self.point_list.alpha - self.alpha_coef
        self.point_list.markersize = self.point_list.markersize + self.size_coef

    def setAlphaCoef(self, alpha_coef):
        self.alpha_coef = alpha_coef

    def setSizeCoef(self, size_coef):
        self.size_coef = size_coef
