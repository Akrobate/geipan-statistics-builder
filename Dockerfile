FROM continuumio/miniconda3

# RUN conda create --name python36 python=3.6

# RUN conda install --name python36 -c conda-forge jupyterlab
# RUN conda install --name python36 -c conda-forge notebook

# RUN conda install --name python36 cartopy
# RUN conda install --name python36 pandas
# RUN conda install --name python36 numpy
# RUN conda install --name python36 seaborn
# RUN conda install --name python36 scikit-learn
# RUN conda install --name python36 pytest
# RUN conda install --name python36 -c menpo opencv

# RUN conda run --name python36 python --version

# Testing
COPY ./conda_requirements.txt ./conda_requirements.txt
RUN conda env create --name python36 --file conda_requirements.txt 

RUN mkdir /geipan-statistics-builder

WORKDIR /geipan-statistics-builder
