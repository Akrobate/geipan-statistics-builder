import pandas as pd

from src.geipan_data import GeipanData
from src.map_image_generator import MapImageGenerator
from src.image_points_data_holder import ImagePointsDataHolder
from src.timeline_sequencer import TimelineSequencer
from src.main_process import mainProcess

source_file = './data/geipan/temoignages_pub.csv'
images_folder = './data/images/'
video_file = './data/videos/geipan.avi'

def main():
    main_process = mainProcess(
        source_file,
        images_folder,
        video_file
    )

    # Optionnal params
    main_process.setSequencePeriod('Y')
    main_process.setDateRange('1970', '2020')
    main_process.setVideoFps(15)

    main_process.cleanPreviousImageGeneration()
    main_process.generateImagesFromData()
    main_process.generateVideoFromImages()

if __name__ == "__main__":
    main()
