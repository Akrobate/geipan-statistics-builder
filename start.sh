#!/bin/bash

DOCKER_IMAGE_NAME="geipan-statistics-builder"

COMMAND_START="ls"

docker build -t $DOCKER_IMAGE_NAME .
docker run -v `pwd`/:/geipan-statistics-builder -i -t $DOCKER_IMAGE_NAME bash -c "$COMMAND_START"

